package a03a.e1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class ExamsManagementImpl implements ExamsManagement {

    Set<Pair<Integer, String>> students = new HashSet<>();
    Map<Pair<String, Integer>,Set<Integer>> exams = new HashMap<>();
    
    public ExamsManagementImpl() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void createStudent(int studentId, String name) {
        // TODO Auto-generated method stub
            Pair<Integer , String> student  = new Pair<>(studentId, name);
            students.add(student);
    }

    @Override
    public void createExam(String examName, int incrementalId) {
        // TODO Auto-generated method stub
        Pair<String, Integer> exam = new Pair<>(examName, incrementalId);
        exams.put(exam, new HashSet<>());
    }

    @Override
    public void registerStudent(String examName, int studentId) {
        exams.entrySet().stream().map(e->e.getKey().getX().equals(examName)).)
        
    }

    @Override
    public void examStarted(String examName) {
        // TODO Auto-generated method stub

    }

    @Override
    public void registerEvaluation(int studentId, int evaluation) {
        // TODO Auto-generated method stub

    }

    @Override
    public void examFinished() {
        // TODO Auto-generated method stub

    }

    @Override
    public Set<Integer> examList(String examName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Optional<Integer> lastEvaluation(int studentId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<String, Integer> examStudentToEvaluation(String examName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<Integer, Integer> examEvaluationToCount(String examName) {
        // TODO Auto-generated method stub
        return null;
    }

}
